# demo Testing

## why testing
    - Automated checking of the code
    - catch bugs quicker
    - easy to debug
    - write better code
    - long term develop faster
    - Improves CI/CD workflow
    - reduces fear

## first test
    - find test
        - file and folder structure
    - run test
    - assert -> try to keep one 
    - use classes
    - no if's
    - no for's

## pytest vs testcase
    - more functionallity
    - better output
    - less code

## fixtures
    - reuse code
    - yield
    - scope
    - global

## marks
    - mark
    - only markers
    - exclude
    - add

## parameters
    - expect
    - alternative for loop

## mocking
    - why mocking
    - what is mock
    - how

## patching
    - path to patching

## raises
   - tets error
   - no if's

