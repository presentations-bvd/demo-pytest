import pytest


@pytest.fixture(scope="class")
def build_object_1():
    print(" \n CREATE FIXTURE build_object_1 \n")
    yield {"foo": 1, "bar": 4}
    print(" \n DESTROY FIXTURE build_object_1 \n")


@pytest.fixture()
def build_object_2():
    print(" \n CREATE FIXTURE build_object_2 \n")
    yield {"foo": 2, "bar": 4}
    print(" \n DESTROY FIXTURE build_object_2 \n")


class TestFixt():

    def test_fixture_foo2(self, build_object_2):

        assert build_object_2["foo"] == 2

    def test_fixture_foo(self, build_object_1):

        assert build_object_1["foo"] == 1

    def test_fixture_bar(self, build_object_1):

        assert build_object_1["bar"] == 4

    def test_fixture_bar2(self, build_object_2):

        assert build_object_2["bar"] == 4


def test_global_fixture(global_fixture):

    assert global_fixture["global"] == 10
