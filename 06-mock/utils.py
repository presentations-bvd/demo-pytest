import json

import logging
import requests
import base64

url = "https://api.ofsomething/"

param = {"foo": "bar"}


class DocfillUtils(object):

    requests = requests

    def post(self, url, param):
        """
        Create and return just the id
        """

        result = self.requests.post(
            url,
            json.dumps(param),
            headers={"Content-Type": "application/json", "Authorization": "Token "})

        return result.json()

    def retrieve(self, entity_type, entity_id):
        return self.get(f"/{entity_type}/{entity_id}/")

    def get(self, url):
        """
        get request to docfill
        """

        result = self.requests.get(url,  headers={
            "Content-Type": "application/json",
            "Authorization": "Token "
        })
        return result.json()

