from docfill_document import DocFillDocument
from unittest.mock import MagicMock


def test_mock():
    doc_utils_mock = MagicMock()
    doc_utils_mock.retrieve.return_value = '{ "bar":"foo" } '

    docfill_document = DocFillDocument("investor", 5)
    docfill_document.docfill_util = doc_utils_mock

    docfill_document.get_document_info()

    doc_utils_mock.retrieve.assert_called_once()
