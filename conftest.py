import pytest


@pytest.fixture(scope="session")
def global_fixture():
    print("CREATE FIXTURE: global fixture")

    yield {"foo": 1, "bar": 4, "global": 10}

    print("DESTROY FIXTURE: global fixture")

