from utils import DocfillUtils


class DocFillDocument():
    """
    Wrapper class for an docfill intenty
    this intenty lives in the microservice docfill
    """

    docfill_util = DocfillUtils()

    def __init__(self, entity_type: str, entity_id: int):
        self.__base64_file_content = None
        self.__docfill_info = None
        self._entity_type = entity_type
        self._entity_id = entity_id

    def retrieve_document_info(self):
        response = self.docfill_util.retrieve(
            self._entity_type, self._entity_id)
        self.__docfill_info == response

    def get_document_info(self):
        if self.__docfill_info is None:
            self.retrieve_document_info()
        return self.__docfill_info

    @property
    def id(self):
        docfill_info = self.get_document_info()
        return docfill_info.get('id')

    @property
    def name(self):
        docfill_info = self.get_document_info()
        return docfill_info.get('name')

    @property
    def file_extension(self):
        docfill_info = self.get_document_info()
        return docfill_info.get('file_extension')
