import pytest


@pytest.mark.parametrize("test_input,expected", [
    ("3+5", 8),
    ("2+4", 6),
    ("6*9", 54)
])
def test_eval(test_input, expected):
    assert eval(test_input) == expected


def add(number1, number2):
    return number1 + number2


@pytest.mark.parametrize("number1, number2,expected", [
    (3, 5, 8),
    (3, 3, 6),
    (40, 5, 45)
])
def test_eval2(number1, number2, expected):
    assert add(number1, number2) == expected
