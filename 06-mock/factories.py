import base64
from django.conf import settings
from .docfill_document import DocFillDocument


base_dir = settings.PROJECT_DIR


def convert_to_base64(document_path: str):
    with open(document_path, "rb") as file:
        content_bytes = file.read()
    base64_file_content = base64.b64encode(content_bytes).decode('ascii')
    return base64_file_content


def dummy_docfill_document_factory():
    docfill = DocFillDocument()
    test_document_path = base_dir + '/docusign/tests/test_documents/Dummy Contract.pdf'
    docfill.base64_file_content = convert_to_base64(test_document_path)
    return docfill
