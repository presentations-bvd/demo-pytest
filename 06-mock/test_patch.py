from docfill_document import DocFillDocument
from unittest.mock import MagicMock, patch


def test_mock():

    with patch('docfill_document.DocfillUtils.retrieve') as mock:
        mock.return_value = '{ "bar":"foo" } '

        docfill_document = DocFillDocument("investor", 5)

        docfill_document.get_document_info()

        mock.assert_called_once_with("investor", 5)


@patch('docfill_document.DocfillUtils.retrieve')
def test_mock_dec(mock):

    mock.return_value = '{ "bar":"foo" } '

    docfill_document = DocFillDocument("investor", 5)

    docfill_document.get_document_info()

    mock.assert_called_once_with("investor", 5)
