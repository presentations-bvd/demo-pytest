from first import hello, func_hello_world


def test_hello_world():

    result = func_hello_world()
    assert isinstance(result, str)
    assert result == "hello world"

    # assert result == "hello what?"
